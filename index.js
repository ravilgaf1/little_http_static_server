import Fastify from 'fastify';
import { config } from './cooper.config.js';
import fs from 'fs/promises';
import path from 'path';
import { fileURLToPath } from 'url';

const fastify = Fastify();

async function getPageCooperFiles(dir) {
	let files = [];
	const items = await fs.readdir(dir, { withFileTypes: true });
	for (const item of items) {
		const fullPath = path.join(dir, item.name);
		if (item.isDirectory()) {
			const pageFile = path.join(fullPath, 'page.html');
			try {
				await fs.access(pageFile, fs.constants.F_OK);
				files.push(pageFile);
			} catch {
				console.log(`page.html not found in ${fullPath}`);
			}
		} else if (item.isFile() && item.name === 'page.html') {
			files.push(fullPath);
		}
	}
	return files;
}

async function setupRoutes() {
	const directoryPath = path.join(path.dirname(fileURLToPath(import.meta.url)), 'pages');
	const files = await getPageCooperFiles(directoryPath);
	for (const file of files) {
		let routePath = file.replace(new RegExp(`^${directoryPath}`), '').replace(/\/page\.html$/, '');
		if (routePath === '') {
			routePath = '/';  // Handling root route
		}
		if (!routePath.startsWith('/')) {
			routePath = '/' + routePath;
		}
		fastify.get(routePath, async (request, reply) => {
			// Reading and returning file content
			try {
				const content = await fs.readFile(file, 'utf8');
				reply.type("text/html").send(content);
			} catch (err) {
				reply.code(500).send('Error reading file');
			}
		});
	}
}

async function startServer() {
	try {
		await setupRoutes();
		await fastify.listen({ port: config.port });
		console.log(`Server is running at http://localhost:${config.port}`);
	} catch (err) {
		console.error('Server start failed:', err);
		process.exit(1);
	}
}

startServer();
